var option = document.getElementsByName("spisok");
var r = document.querySelectorAll('div #radios');
var ch = document.querySelectorAll('div #checkboxes');
var spisokCen = {
  chekB_cena: {
    cena1: 5000,
    cena2: 10000,
    cena3: 20000,
  },
  radio_cena: {
    r1: 500,
    r2: 0,
  }
};
var Cena =10000;
var sum = 0;
function cena(){
  switch(option[0].value)
  {
    case "Хаска" :
        Cena = 10000;
        for(let i = 0; i<r.length; i++)
          { r[i].style.display = "none"; }
        for(let i = 0; i<ch.length; i++)
          { ch[i].style.display = "none"; }
        break;
    case "Эстонская гончая" :
        Cena = 15000;
        for(let i = 0; i<r.length; i++)
          { r[i].style.display = "inline-block"; }
        for(let i = 0; i<ch.length; i++)
          { ch[i].style.display = "none"; }
        break;
    case "Ротвейлер" :
        Cena = 20000;
        for(let i = 0; i<r.length; i++)
          { r[i].style.display = "none"; }
        for(let i = 0; i<ch.length; i++)
          { ch[i].style.display = "inline-block"; }
        break;
  }
}

function price() {
   
  var k = document.getElementsByName("kolvo");
  var rr = document.getElementById("result");
  var res = k[0].value.match(/^[0-9]*$/);
  if( res !== null)
   { 
      res = k[0].value * Cena; 
      sum+=res; 
      rr.innerHTML = res;
   }
  else { alert("Неправильный ввод"); }
}
function summ(){
  var r2 = document.getElementById("Sum");
  r2.innerHTML = sum;
}

window.addEventListener('DOMContentLoaded', function (event) {
  console.log("DOM fully loaded and parsed");
  var b = document.getElementById("button");
  var c = document.getElementById("SumB");


  for(let i = 0; i<r.length; i++)
  { r[i].style.display = "none"; }
  for(let i = 0; i<ch.length; i++)
  { ch[i].style.display = "none"; }

  option[0].addEventListener("click", cena);
 
  b.addEventListener("click", price);
  c.addEventListener("click", summ);


  r.forEach(function(radio){
    radio.addEventListener("click", function(event){
        let tr = spisokCen.radio_cena[event.target.value];
        if(tr !== undefined){
         Cena+=tr;
         price();
         cena();
        }
    })
  })

  ch.forEach(function(checkbox){
    checkbox.addEventListener("click", function(event){
      if(event.target.checked){
        let tc = spisokCen.chekB_cena[event.target.name];
        if(tc !== undefined){
          Cena+=tc;
          price();
        }
      }
      else { let tc = spisokCen.chekB_cena[event.target.name];
        if(tc !== undefined){
          Cena-=tc;
          price();
        } 
      }
    })
  })


});
